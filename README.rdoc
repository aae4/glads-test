== README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...


Please feel free to use a different markup language if you do not plan to run
<tt>rake doc:app</tt>.

1) rake db:seed

Api Examples:
-------------
- SIGN-IN
  request: curl -X POST --data "user[login]=admin&user[password]=123456" http://localhost:3000/api/v1/sign-in
  response: {"auth_token": token}

- Get list of dishes
  request: curl -X GET http://localhost:3000/api/v1/dishes?user_token=#{token}
  params: category_id
  response: {@dishes}

- SIGN-OUT
  request: curl -X DELETE --data "user_token=#{token}" http://localhost:3000/api/v1/sign-out

- Create order:
  request: curl -X POST --data "order[dish_ids][]=1&order[dish_ids]=2" http://localhost:3000/api/v1/orders?user_token=#{token}
