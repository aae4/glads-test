Rails.application.routes.draw do
  devise_for :users
  root "admin/categories#index"

  scope 'admin', module: 'admin', :as => "admin" do
    resources :categories
    resources :dishes
    resources :orders, only: :index do
      member do
        get :dishes_list
      end
    end
    resources :reports, only: :index
  end

  namespace :api, defaults: {format: 'json'} do
    namespace :v1 do
      resources :dishes
      resources :orders, only: :create
      post   "/sign-in"       => "sessions#create"
      delete "/sign-out"      => "sessions#destroy"
    end
  end
end
