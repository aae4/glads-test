module Dishes
  class GetListService

    def self.build(params)
      return Dish.by_params(params).map{|d| {id: d.id, name: d.name, category_name: d.category_name}}
    end

  end
end
