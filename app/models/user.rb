class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  #:recoverable, :trackable,
  has_many :orders

  devise :database_authenticatable, :registerable,
         :rememberable, :validatable

  validates_presence_of :login
  validates_uniqueness_of :login

  before_save :ensure_authentication_token

  def ensure_authentication_token
    if authentication_token.blank?
      self.authentication_token = generate_authentication_token
    end
  end

  def email_required?
    false
  end

  def email_changed?
    false
  end

  def invalidate_auth_token
    self.update_columns(authentication_token: generate_authentication_token)
  end

  private

  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless User.where(authentication_token: token).first
    end
  end
end
