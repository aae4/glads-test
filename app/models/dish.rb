class Dish < ActiveRecord::Base
  belongs_to :category
  has_many :dishes_orders
  has_and_belongs_to_many :orders

  validates_presence_of :name, :category_id, :price

  class << self
    def by_params(params)
      dishes = Dish.includes(:category)
      dishes = dishes.where(category: params[:category_id]) unless params[:category_id].blank?
      dishes.order("categories.name")
    end
  end

  def category_name
    category.name || ""
  end
end
