class Order < ActiveRecord::Base
  belongs_to :user
  has_many :dishes_orders
  has_and_belongs_to_many :dishes
end
