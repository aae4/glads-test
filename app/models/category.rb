class Category < ActiveRecord::Base
  has_many :dishes
  validates_uniqueness_of :name
  validates_presence_of :name

  has_many :children, class_name: "Category", foreign_key: "parent_id"
  belongs_to :parent, class_name: "Category", foreign_key: "parent_id"

  scope :roots, -> { where(parent_id: nil) }

  def full_name
    return name if parent.blank?
    parent.full_name + "/#{name}"
  end
end
