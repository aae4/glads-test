class Admin::DishesController < Admin::BaseController
  before_action :set_dishes, only: [:index, :create, :update, :destroy]

  def index
  end

  def edit
    @dish = Dish.find(params[:id])
  end

  def update
    @dish = Dish.find(params[:id])

    if @dish.update_attributes(dish_params)
      render :refresh_dishes
    else
      render :action => 'edit'
    end
  end

  def create
    @dish = Dish.new dish_params

    if @dish.save
      render :refresh_dishes
    else
      render action: :new
    end
  end

  def new
    @dish = Dish.new
  end

  def show
    @dish = Dish.find(params[:id])
  end

  def destroy
    @dish = Dish.find(params[:id])
    @dish.destroy
    render :refresh_dishes
  end

  private
    def set_dishes
      @dishes = Dish.includes(:category).order("categories.name").page(params[:page])
    end

    def dish_params
      params.require(:dish).permit(:name, :category_id, :price)
    end
end
