class Admin::OrdersController < Admin::BaseController

  def index
    @orders = Order.order("created_at DESC").page(params[:page])
  end

  def dishes_list
    order = Order.find_by(id: params[:id])
    @dishes = order.dishes
  end
end
