class Admin::CategoriesController < Admin::BaseController
  before_action :set_categories, only: [:index, :create, :update, :destroy]

  def index
  end

  def edit
    @category = Category.find(params[:id])
  end

  def update
    @category = Category.find(params[:id])

    if @category.update_attributes(category_params)
      render :refresh_categories
    else
      render :action => 'edit'
    end
  end

  def create
    @category = Category.new category_params

    if @category.save
      render :refresh_categories
    else
      render action: :new
    end
  end

  def new
    @category = Category.new
  end

  def destroy
    @category = Category.find(params[:id])
    @category.destroy
    render :refresh_categories
  end

  private
    def set_categories
      @categories = Category.order("created_at DESC").page(params[:page])
    end

    def category_params
      params.require(:category).permit(:name,:parent_id)
    end
end
