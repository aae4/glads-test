class Admin::ReportsController < Admin::BaseController

  def index
    #TODO: cache orders count
    @dishes = Dish.joins(:orders).group("dishes.id").order("count(orders.id) DESC").page(params[:page])
  end
end
