module Api
  module V1
    class DishesController < Api::BaseController
      respond_to :json

      # GET /api/v1/dishes
      # params:
      #   ?category_id=1
      def index
        render json: ::Dishes::GetListService.build(params)
      end

    end
  end
end
