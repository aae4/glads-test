module Api
  module V1
    class OrdersController < Api::BaseController
      respond_to :json

      #curl -X POST --data "order[dish_ids][]=1&order[dish_ids]=2" http://localhost:3000/api/v1/orders?user_token=#{token}
      def create
        order = Order.new order_params
        order.user_id = current_user.id

        respond_to do |format|
          if order.save
            format.json {render json: {status: "ok"}}
          else
            format.json {render json: {status: 401}}
          end
        end
      end

      private
        def order_params
          params.require(:order).permit(:dish_ids => [])
        end

    end
  end
end
