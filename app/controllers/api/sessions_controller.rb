class Api::SessionsController < Api::BaseController
  skip_before_action :authenticate_user!, only: [:create]

  #curl -X POST --data "user[login]=admin&user[password]=123456" http://localhost:3000/api/v1/sign-in
  def create
    resource = User.find_for_database_authentication(:login => params[:user][:login])
    return invalid_login unless resource

    if resource.valid_password?(params[:user][:password])
      auth_token = resource.authentication_token
      render json: { auth_token: auth_token }
    else
      invalid_login
    end

  end

  #curl -X DELETE --data "user_token=#{token}" http://localhost:3000/api/v1/sign-out
  def destroy
    resource = current_user
    resource.invalidate_auth_token
    head :ok
  end

  private
    def invalid_login
      render json: { errors: [ { detail: "Invalid login or password" }]}, status: 401
    end
end
