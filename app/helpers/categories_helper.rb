module CategoriesHelper
  def category_options(object)
    Category.where.not(id: object.id)
  end
end
