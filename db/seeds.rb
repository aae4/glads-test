# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create(login: "admin", password: "123456", password_confirmation: "123456") if !User.find_by(login: "admin")

dishes = {
  "Горячее" => ["Баранина", "Стейк с кровью", "Борщ"],
  "Десерты" => ["Тирмаису", "Наполеон", "Запеканка"],
  "Напитки" => ["Чай", "Кофе", "Морс"]
}

dishes.each do |category_name, ds|
  c = Category.find_or_create_by(name: category_name)
  ds.each{|d| Dish.find_or_create_by(category_id: c.id, name: d)}
end

Dish.all.each{|d| d.update_attributes(price: rand(800))}
