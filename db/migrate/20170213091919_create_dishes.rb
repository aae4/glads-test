class CreateDishes < ActiveRecord::Migration
  def change
    create_table :dishes do |t|
      t.string :name
      t.integer :category_id
      t.float :price,  null: false, default: 0

      t.timestamps null: false
    end
  end
end
